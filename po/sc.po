# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: gallery-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-22 21:06+0000\n"
"PO-Revision-Date: 2019-01-20 00:00+0000\n"
"Last-Translator: Adrià <adriamartinmor@gmail.com>\n"
"Language-Team: Sardinian <https://translate.ubports.com/projects/ubports/"
"gallery-app/sc/>\n"
"Language: sc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.1.1\n"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:43
#: rc/qml/AlbumEditor/AlbumEditor.qml:36
msgid "Edit album"
msgstr "Modìfica album"

#: rc/qml/AlbumEditor/AlbumEditMenu.qml:50
#: rc/qml/AlbumViewer/AlbumViewer.qml:378 rc/qml/Components/DeleteDialog.qml:32
#: rc/qml/EventsOverview.qml:139 rc/qml/MediaViewer/MediaViewer.qml:350
#: rc/qml/PhotosOverview.qml:195 rc/qml/Utility/EditingHUD.qml:66
msgid "Delete"
msgstr "Cantzella"

#: rc/qml/AlbumEditor/AlbumEditor.qml:39 rc/qml/Components/MediaSelector.qml:93
#: rc/qml/EventsOverview.qml:161 rc/qml/MediaViewer/MediaViewer.qml:278
#: rc/qml/PhotosOverview.qml:218 rc/qml/PickerScreen.qml:285
msgid "Cancel"
msgstr "Annulla"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:113
msgid "Default"
msgstr "Predefinidu"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:114
msgid "Blue"
msgstr "Biaitu"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:115
msgid "Green"
msgstr "Birde"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:116
msgid "Pattern"
msgstr "Motivu"

#: rc/qml/AlbumViewer/AlbumCoverList.qml:117
msgid "Red"
msgstr "Ruju"

#: rc/qml/AlbumViewer/AlbumViewer.qml:107 rc/qml/MainScreen.qml:62
msgid "Album"
msgstr "Album"

#: rc/qml/AlbumViewer/AlbumViewer.qml:372
#: rc/qml/MediaViewer/MediaViewer.qml:340
msgid "Add to album"
msgstr "Agiunghe a s'album"

#: rc/qml/AlbumsOverview.qml:229 rc/qml/Components/PopupAlbumPicker.qml:91
msgid "Add new album"
msgstr "Agiunghe un'album nou"

#: rc/qml/AlbumsOverview.qml:233 rc/qml/Components/PopupAlbumPicker.qml:101
msgid "New Photo Album"
msgstr "Album de fotografias nou"

#: rc/qml/AlbumsOverview.qml:234 rc/qml/Components/PopupAlbumPicker.qml:102
msgid "Subtitle"
msgstr "Sutatìtulu"

#: rc/qml/AlbumsOverview.qml:241 rc/qml/EventsOverview.qml:116
#: rc/qml/PhotosOverview.qml:173
msgid "Camera"
msgstr "Fotocàmera"

#: rc/qml/Components/DeleteDialog.qml:36 rc/qml/MediaViewer/MediaViewer.qml:226
msgid "Yes"
msgstr "Eja"

#: rc/qml/Components/DeleteDialog.qml:45 rc/qml/MediaViewer/MediaViewer.qml:237
msgid "No"
msgstr "Nono"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:51
msgid "Delete album"
msgstr "Cantzella s'album"

#: rc/qml/Components/DeleteOrDeleteWithContentsDialog.qml:60
msgid "Delete album AND contents"
msgstr "Cantzella s'album e is contenutos"

#: rc/qml/Components/EventCard.qml:59
msgid "MMM yyyy"
msgstr "MMM yyyy"

#: rc/qml/Components/EventCard.qml:73
msgid "dd"
msgstr "dd"

#: rc/qml/Components/MediaSelector.qml:59
#: rc/qml/Components/MediaSelector.qml:81
msgid "Add to Album"
msgstr "Agiunghe a s'album"

#: rc/qml/Components/PopupAlbumPicker.qml:162 rc/qml/Utility/EditingHUD.qml:77
msgid "Add Photo to Album"
msgstr "Agiunghe sa fotografia a s'album"

#: rc/qml/Components/UnableShareDialog.qml:25
msgid "Unable to share"
msgstr "Non faghet a cumpartzire"

#: rc/qml/Components/UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr "Non faghet a cumpartzire fotografias e vìdeos impares"

#: rc/qml/Components/UnableShareDialog.qml:30
msgid "Ok"
msgstr "AB"

#: rc/qml/EventsOverview.qml:80 rc/qml/PhotosOverview.qml:87
#, qt-format
msgid "Delete %1 photo"
msgid_plural "Delete %1 photos"
msgstr[0] "Boles cantzellare %1 fotografia?"
msgstr[1] "Boles cantzellare %1 fotografias?"

#: rc/qml/EventsOverview.qml:86 rc/qml/PhotosOverview.qml:93
#, qt-format
msgid "Delete %1 video"
msgid_plural "Delete %1 videos"
msgstr[0] "Boles cantzellare %1 vìdeu?"
msgstr[1] "Boles cantzellare %1 vìdeos?"

#: rc/qml/EventsOverview.qml:92 rc/qml/PhotosOverview.qml:99
#, qt-format
msgid "Delete %1 media file"
msgid_plural "Delete %1 media files"
msgstr[0] "Boles cantzellare %1 cuntenutu multimediale?"
msgstr[1] "Boles cantzellare %1 cuntenutos multimediales?"

#: rc/qml/EventsOverview.qml:109 rc/qml/MainScreen.qml:199
#: rc/qml/MainScreen.qml:251 rc/qml/PhotosOverview.qml:166
#: rc/qml/PickerScreen.qml:192 rc/qml/PickerScreen.qml:239
msgid "Select"
msgstr "Seletziona"

#: rc/qml/EventsOverview.qml:128 rc/qml/PhotosOverview.qml:185
#: rc/qml/Utility/EditingHUD.qml:76
msgid "Add"
msgstr "Agiunghe"

#: rc/qml/EventsOverview.qml:146 rc/qml/MediaViewer/MediaViewer.qml:361
#: rc/qml/PhotosOverview.qml:202 rc/qml/Utility/EditingHUD.qml:71
msgid "Share"
msgstr "Cumpartzi"

#: rc/qml/EventsOverview.qml:195 rc/qml/MediaViewer/MediaViewer.qml:182
#: rc/qml/PhotosOverview.qml:241
msgid "Share to"
msgstr "Cumpartzi cun"

#: rc/qml/LoadingScreen.qml:39
msgid "Loading…"
msgstr "Carrighende…"

#: rc/qml/MainScreen.qml:148
msgid "Albums"
msgstr "Albums"

#: rc/qml/MainScreen.qml:162 rc/qml/MainScreen.qml:201
#: rc/qml/PickerScreen.qml:154
msgid "Events"
msgstr "Eventos"

#: rc/qml/MainScreen.qml:210 rc/qml/MainScreen.qml:253
#: rc/qml/PickerScreen.qml:203
msgid "Photos"
msgstr "Fotografias"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a photo"
msgstr "Cantzella una fotografia"

#: rc/qml/MediaViewer/MediaViewer.qml:215
msgid "Delete a video"
msgstr "Cantzella unu vìdeu"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a photo from album"
msgstr "Fùlia una fotografia dae s'album"

#: rc/qml/MediaViewer/MediaViewer.qml:248
msgid "Remove a video from album"
msgstr "Fùlia unu vìdeu dae s'album"

#: rc/qml/MediaViewer/MediaViewer.qml:257
msgid "Remove from Album"
msgstr "Fùlia dae s'album"

#: rc/qml/MediaViewer/MediaViewer.qml:268
msgid "Remove from Album and Delete"
msgstr "Fùlia dae s'album e dae su dispositivu"

#: rc/qml/MediaViewer/MediaViewer.qml:315
msgid "Edit"
msgstr "Modìfica"

#: rc/qml/MediaViewer/MediaViewer.qml:371
msgid "Info"
msgstr "Informatziones"

#: rc/qml/MediaViewer/MediaViewer.qml:394
msgid "Informations"
msgstr "Informatziones"

#: rc/qml/MediaViewer/MediaViewer.qml:397
msgid "Media type: "
msgstr "Genia de cuntenutu multimediale: "

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "photo"
msgstr "Fotografias"

#: rc/qml/MediaViewer/MediaViewer.qml:398
msgid "video"
msgstr "Vìdeu"

#: rc/qml/MediaViewer/MediaViewer.qml:399
msgid "Media name: "
msgstr "Nòmine de su cuntenutu multimediale: "

#: rc/qml/MediaViewer/MediaViewer.qml:400
msgid "Date: "
msgstr "Data: "

#: rc/qml/MediaViewer/MediaViewer.qml:401
msgid "Time: "
msgstr "Ora: "

#: rc/qml/MediaViewer/MediaViewer.qml:407
msgid "ok"
msgstr "AB"

#: rc/qml/MediaViewer/PhotoEditorPage.qml:26
msgid "Edit Photo"
msgstr "Modìfica sa fotografia"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:46
#: desktop/gallery-app.desktop.in.in.h:1
msgid "Gallery"
msgstr "Galleria"

#: rc/qml/MediaViewer/PopupPhotoViewer.qml:183
msgid "Toggle Selection"
msgstr "Fùrria sa seletzione"

#: rc/qml/MediaViewer/SingleMediaViewer.qml:241
msgid "An error has occurred attempting to load media"
msgstr "B'est istada una faddina in su carrigamentu de s'archìviu multimediale"

#: rc/qml/PhotosOverview.qml:116 rc/qml/PhotosOverview.qml:159
msgid "Grid Size"
msgstr "Mannària de sa grìllia"

#: rc/qml/PhotosOverview.qml:117
msgid "Select the grid size in gu units between 8 and 20 (default is 12)"
msgstr ""
"Sèbera sa mannària de sa grìllia in unidades gus, intre 8 e 20 (predefinidu: "
"12)"

#: rc/qml/PhotosOverview.qml:132
msgid "Finished"
msgstr "Fatu"

#: rc/qml/PickerScreen.qml:291
msgid "Pick"
msgstr "Sèbera"

#: rc/qml/Utility/EditingHUD.qml:67
msgid "Trash;Erase"
msgstr "Àliga;Cantzella"

#: rc/qml/Utility/EditingHUD.qml:72
msgid "Post;Upload;Attach"
msgstr "Pùblica;Càrriga;Agiunghe"

#: rc/qml/Utility/EditingHUD.qml:81
msgid "Undo"
msgstr "Iscontza"

#: rc/qml/Utility/EditingHUD.qml:82
msgid "Cancel Action;Backstep"
msgstr "Annulla s'atzione;Torra in segus"

#: rc/qml/Utility/EditingHUD.qml:86
msgid "Redo"
msgstr "Torra a fàghere"

#: rc/qml/Utility/EditingHUD.qml:87
msgid "Reapply;Make Again"
msgstr "Torra a aplicare;Torra a fàghere"

#: rc/qml/Utility/EditingHUD.qml:91
msgid "Auto Enhance"
msgstr "Megioramentu automàticu"

#: rc/qml/Utility/EditingHUD.qml:92
msgid "Adjust the image automatically"
msgstr "Regulamentu automàticu de s'imàgine"

#: rc/qml/Utility/EditingHUD.qml:93
msgid "Automatically Adjust Photo"
msgstr "Regulamentu automàticu de sa fotografia"

#: rc/qml/Utility/EditingHUD.qml:98
msgid "Rotate"
msgstr "Gira"

#: rc/qml/Utility/EditingHUD.qml:99
msgid "Turn Clockwise"
msgstr "Gira in sensu oràriu"

#: rc/qml/Utility/EditingHUD.qml:100
msgid "Rotate the image clockwise"
msgstr "Gira s'imàgine in sensu oràriu"

#: rc/qml/Utility/EditingHUD.qml:105
msgid "Crop"
msgstr "Sega"

#: rc/qml/Utility/EditingHUD.qml:106
msgid "Trim;Cut"
msgstr "Rafila;Sega"

#: rc/qml/Utility/EditingHUD.qml:107
msgid "Crop the image"
msgstr "Sega s'imàgine"

#: rc/qml/Utility/EditingHUD.qml:112
msgid "Revert to Original"
msgstr "Riprìstina s'originale"

#: rc/qml/Utility/EditingHUD.qml:113
msgid "Discard Changes"
msgstr "Non sarves is modìficas"

#: rc/qml/Utility/EditingHUD.qml:114
msgid "Discard all changes"
msgstr "Non sarves is modìficas"

#: rc/qml/Utility/EditingHUD.qml:118
msgid "Exposure"
msgstr "Espositzione"

#: rc/qml/Utility/EditingHUD.qml:119
msgid "Adjust the exposure"
msgstr "Regulamentu de s'espositzione"

#: rc/qml/Utility/EditingHUD.qml:120
msgid "Underexposed;Overexposed"
msgstr "Sutaespositzione;Subraespositzione"

#: rc/qml/Utility/EditingHUD.qml:121 rc/qml/Utility/EditingHUD.qml:162
msgid "Confirm"
msgstr "Cunfirma"

#: rc/qml/Utility/EditingHUD.qml:125
msgid "Compensation"
msgstr "Cumpensatzione"

#: rc/qml/Utility/EditingHUD.qml:159
msgid "Color Balance"
msgstr "Bilantzamentu de su colore"

#: rc/qml/Utility/EditingHUD.qml:160
msgid "Adjust color balance"
msgstr "Acontza su bilantzu de colore"

#: rc/qml/Utility/EditingHUD.qml:161
msgid "Saturation;Hue"
msgstr "Saturatzione;Tonalidade"

#: rc/qml/Utility/EditingHUD.qml:166
msgid "Brightness"
msgstr "Lugore"

#: rc/qml/Utility/EditingHUD.qml:177
msgid "Contrast"
msgstr "Cuntrastu"

#: rc/qml/Utility/EditingHUD.qml:188
msgid "Saturation"
msgstr "Saturatzione"

#: rc/qml/Utility/EditingHUD.qml:199
msgid "Hue"
msgstr "Tonalidade"

#: desktop/gallery-app.desktop.in.in.h:2
msgid "Ubuntu Photo Viewer"
msgstr "Visualizadore de fotografias de Ubuntu"

#: desktop/gallery-app.desktop.in.in.h:3
msgid "Browse your photographs"
msgstr "Isfògia is fotografias"

#: desktop/gallery-app.desktop.in.in.h:4
msgid "Photographs;Pictures;Albums"
msgstr "Fotografias;Immàgines;Albums"

#~ msgid "Delete 1 photo"
#~ msgstr "Cantzella sa fotografia"

#~ msgid "Delete 1 video"
#~ msgstr "Cantzella su vìdeu"

#~ msgid "Delete %1 photos and 1 video"
#~ msgstr "Cantzella %1 fotografias e 1 vìdeu"

#~ msgid "Delete 1 photo and %1 videos"
#~ msgstr "Cantzella 1 fotografia e %1 vìdeos"

#~ msgid "Delete 1 photo and 1 video"
#~ msgstr "Cantzella 1 fotografia e 1 vìdeu"

#~ msgid "Delete %1 photos and %2 videos"
#~ msgstr "Cantzella %1 fotografias e %2 vìdeos"
